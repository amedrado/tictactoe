<?php

namespace App\Domain\Game\Board\Exception;

class FullBoardException extends \Exception
{
    public function __construct()
    {
        $message = 'This board is already complete (max 3 rows)' ;
        parent::__construct($message);
    }
}
