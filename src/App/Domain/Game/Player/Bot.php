<?php

namespace App\Domain\Game\Player;

use App\Domain\Game\Board\Board;
use App\Domain\Game\Minimax;

class Bot implements PlayerInterface
{
    const PLAYER_TYPE_BOT = 'O';

    public function setBoardState(array $boardState): array
    {
        $minimax = new Minimax();
        $boardState = $minimax->makeMove($boardState, self::PLAYER_TYPE_BOT);
        return $boardState;
    }

    public function __toString()
    {
        return self::PLAYER_TYPE_BOT;
    }
}