<?php

namespace AppTest\Domain\Game\Board;


use App\Domain\Game\Board\Cell;
use App\Domain\Game\Board\Exception\FullRowException;
use App\Domain\Game\Board\Row;
use PHPUnit\Framework\TestCase;

class RowTest extends TestCase
{
    public function testRowSuccess()
    {
        $row = new Row();
        $this->assertCount(Row::CELL_BY_ROW_LIMIT, $row->getCells());
    }

    public function testRowFullException()
    {
        $this->expectException(FullRowException::class);
        $row = new Row();
        $row->addCell(new Cell(0));
    }
}