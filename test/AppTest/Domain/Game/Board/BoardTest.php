<?php

namespace AppTest\Domain\Game;

use App\Domain\Game\Board\Board;
use App\Domain\Game\Board\Exception\InvalidCoordinateException;
use App\Domain\Game\Board\Row;
use App\Domain\Game\Player\Bot;
use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    public function testBoard()
    {
        $board = new Board();
        $this->assertCount(3, $board->getRows());
        $this->assertCount(3, $board->getRows()[0]->getCells());
    }

    public function testFullBoardException()
    {
        $this->expectException(\App\Domain\Game\Board\Exception\FullBoardException::class);
        $board = new Board();
        $board->addRow(new Row(4));
    }

    public function testValidateBoardStructure()
    {
        $structure = [["", "O", "X"], ["", "", "X"], ["", "", ""]];
        $this->assertTrue(Board::validateBoardStructure($structure));

    }

    public function testValidateBoardStructureWithIncorretRowNumber()
    {
        $structure = [["", "O", "X"], ["", "", "X"], ["", "", ""],[]];
        $this->assertFalse(Board::validateBoardStructure($structure));
    }

    public function testValidateBoardStructureWithIncorretCellNumber()
    {
        $structure = [["", "O", "X"], ["", "X"], ["", "", ""]];
        $this->assertFalse(Board::validateBoardStructure($structure));
    }

    public function testHasAvailableSpaces()
    {
        $structure = [["", "O", "X"], ["", "", "X"], ["", "", ""]];
        $board = new Board();
        $board->updateState($structure);
        $this->assertTrue($board->hasAvailableSpaces());
    }

    public function testNoAvailableSpaces()
    {
        $structure = [["O", "O", "X"], ["X", "X" ,"X"], ["O", "X", "O"]];
        $board = new Board();
        $board->updateState($structure);
        $this->assertFalse($board->hasAvailableSpaces());
    }

    public function testSetPosition()
    {
        $boardState = [["", "O", "X"], ["", "", "X"], ["", "", ""]];
        $boardStateAfter = [["", "O", "X"], ["", "", "X"], ["", "", "O"]];
        $board = new Board();
        $board->updateState($boardState);
        $player = new Bot();
        $board->setPosition(2,2, $player);
        $this->assertEquals($board->toArray(), $boardStateAfter);
    }
}
