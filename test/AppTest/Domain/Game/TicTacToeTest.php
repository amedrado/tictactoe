<?php

namespace AppTest\Domain\Game;

use App\Domain\Game\Board\Exception\InvalidBoardStructureException;
use App\Domain\Game\Board\Row;
use App\Domain\Game\Player\User;
use App\Domain\Game\TicTacToe;
use PHPUnit\Framework\TestCase;

class TicTacToeTest extends TestCase
{
    public function testInstanceCreation()
    {
        $requestParams = $this->prepareFixture();
        $ticTacToe = new TicTacToe($requestParams);
        $this->assertCount(3, $ticTacToe->getLastBoardStateData());
        $totalCells = 0;
        foreach ($ticTacToe->getLastBoardStateData() as $boardRow) {
            $totalCells += count($boardRow);
            $this->assertCount(Row::CELL_BY_ROW_LIMIT, $boardRow);
        }
        $this->assertEquals($totalCells, 9);
    }

    protected function prepareFixture(): array
    {
        $requestParams = [
            "coordinateX" => 1,
            "coordinateY" => 2,
            "player" => "X",
            "currentBoardState" => [["", "", ""], ["", "X", ""], ["", "", ""]]
        ];
        return $requestParams;
    }

    public function testUserPlay()
    {
        $requestParams = $this->prepareFixture();
        $ticTacToe = new TicTacToe($requestParams);
        $initialBoardState = $ticTacToe->getLastBoardStateData();
        $this->assertCount(3, $initialBoardState);
        $totalCells = 0;

        foreach ($initialBoardState as $boardRow) {
            $totalCells += count($boardRow);
            $this->assertCount(Row::CELL_BY_ROW_LIMIT, $boardRow);
        }
        $this->assertEquals($totalCells, 9);
        $ticTacToe->play(new User());
        $this->assertEquals($ticTacToe->getBoard()->toArray(), $initialBoardState);
    }

    public function testPrepareLastBoardStateWithException()
    {
        $this->expectException(InvalidBoardStructureException::class);
        $requestParams = $this->prepareFixture();
        $requestParams['currentBoardState'][2] = ['', '', '', ''];
        new TicTacToe($requestParams);
    }
}
