<?php
$app->get('/v1/api/ping', App\Action\PingAction::class, 'api.ping');
$app->post('/v1/api/move', App\Action\MoveAction::class, 'api.move');
$app->post('/v1/api/move-bot', App\Action\MoveBotAction::class, 'api.move-bot');
